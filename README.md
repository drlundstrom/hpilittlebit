# HPILittleBit

This projected contains files, schematic and BOM for making a human-powered LittleBit. 

## Status
In the process of uploading schematics and Gerber files. 

## BOM
A Mouser [BOM](https://www.mouser.se/ProjectManager/ProjectDetail.aspx?AccessID=52d1727e72) is available to purchase most components. Not included in the BOM is a terminal block (lead distance 5mm) and the LittleBit BitSnaps. The bitsnaps can (e.g.) be purchased from [Sphero](https://sphero.com/products/bitsnaps?_pos=2&_sid=262e6f37e&_ss=r)
